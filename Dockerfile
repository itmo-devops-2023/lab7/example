# Этап сборки
FROM python:3.9-slim AS builder

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

FROM python:3.9-slim AS runner

WORKDIR /app

COPY --from=builder /app /app

EXPOSE 8080

CMD ["python", "app.py"]
